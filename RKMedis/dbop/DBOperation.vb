﻿Imports System.Data.SQLite

Module DBOperation

    Dim connectionString As String = "URI=file:" & Path.ApplicationDBLocation
    Dim con As SQLiteConnection

    Function Varchar(ByVal anParameter As String) As String
        Return "'" & anParameter & "'"
    End Function

    Private Function connection() As SQLiteConnection
        Return con
    End Function

    Sub test()
        Dim con As SQLiteConnection
        Dim cmd As SQLiteCommand

        Try
            Dim cs As String = "Data Source=:memory:"
            con = New SQLiteConnection(cs)
            con.Open()

            Dim stm As String = "SELECT SQLITE_VERSION()"
            cmd = New SQLiteCommand(stm, con)

            Dim version As String = Convert.ToString(cmd.ExecuteScalar())

            MsgBox("SQLite version : " & version)

        Catch ex As SQLiteException
            MsgBox("Something " & ex.Message)
        End Try

    End Sub

    Sub connect()

        If (con IsNot Nothing) Then
            disconnect()
        End If

        con = New SQLiteConnection(connectionString)
        con.Open()

    End Sub

    Sub disconnect()

        If (con IsNot Nothing) Then
            con.Close()
        End If

    End Sub

    Function UpdateUser(ByVal obVal As Users)
        Dim stat As Boolean = False

        Dim kolomSet As String = obVal.getAllColumnsWithValues()
        Dim kolomKondisi As String = obVal.getUsernameColumnWithValue()

        Dim aSQLString As String = "UPDATE users SET " & kolomSet & " WHERE " & kolomKondisi

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString
            aSQLCommand.Prepare()

            ' this is depend upon the parameter (preparedParameter) inside User Class Column
            aSQLCommand.Parameters.Add("@logo", DbType.Binary, obVal.Logo.Length)
            aSQLCommand.Parameters("@logo").Value = obVal.Logo

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function SavePetugas(ByVal obVal As Petugas) As Boolean
        Dim stat As Boolean = False

        Dim kolomSet = obVal.getAllValuesWithoutColumns()

        Dim aSQLString As String = "INSERT INTO petugas VALUES(" & kolomSet & ")"

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString
            aSQLCommand.Prepare()

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function UpdatePetugas(ByVal obVal As Petugas) As Boolean
        Dim stat As Boolean = False

        Dim kolomSet As String = obVal.getAllColumnsWithValues()
        Dim kolomKondisi As String = obVal.getIDColumnWithValue()

        Dim aSQLString As String = "UPDATE petugas SET " & kolomSet & " WHERE " & kolomKondisi

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function DeletePetugas(ByVal obVal As Petugas) As Boolean
        Dim stat As Boolean = False

        Dim kolomKondisi As String = obVal.getIDColumnWithValue()

        Dim aSQLString As String = "DELETE FROM petugas WHERE " & kolomKondisi

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function GetAllPetugas() As ArrayList
        Dim aList As New ArrayList

        Dim satuOrang As New Petugas()


        Dim aSQLString As String = "SELECT * FROM petugas"

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            Dim aReader As SQLiteDataReader = aSQLCommand.ExecuteReader()

            Using aReader
                While aReader.Read()
                    satuOrang.ID = aReader("id")
                    satuOrang.Jabatan = aReader("jabatan")
                    satuOrang.Nama = aReader("nama")
                    satuOrang.NIP = aReader("nip")
                    satuOrang.Telp = aReader("telp")

                    aList.Add(satuOrang)
                    satuOrang = New Petugas()
                End While
            End Using

        End Using

        disconnect()

        Return aList
    End Function

    Function GetAllDokter() As ArrayList
        Dim aList As New ArrayList

        Dim satuOrang As New Dokter()


        Dim aSQLString As String = "SELECT * FROM dokter"

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            Dim aReader As SQLiteDataReader = aSQLCommand.ExecuteReader()

            Using aReader
                While aReader.Read()
                    satuOrang.ID = aReader("id")
                    satuOrang.Kode = aReader("kode")
                    satuOrang.Nama = aReader("nama")
                    satuOrang.NIP = aReader("nip")
                    satuOrang.Spesialis = aReader("spesialis")

                    aList.Add(satuOrang)
                    satuOrang = New Dokter()
                End While
            End Using

        End Using

        disconnect()

        Return aList
    End Function

    Function DeleteDokter(ByVal obVal As Dokter) As Boolean
        Dim stat As Boolean = False

        Dim kolomKondisi As String = obVal.getIDColumnWithValue()

        Dim aSQLString As String = "DELETE FROM dokter WHERE " & kolomKondisi

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function UpdateDokter(ByVal obVal As Dokter) As Boolean
        Dim stat As Boolean = False

        Dim kolomSet As String = obVal.getAllColumnsWithValues()
        Dim kolomKondisi As String = obVal.getIDColumnWithValue()

        Dim aSQLString As String = "UPDATE dokter SET " & kolomSet & " WHERE " & kolomKondisi

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function

    Function SaveDokter(ByVal obVal As Dokter) As Boolean
        Dim stat As Boolean = False

        Dim kolomSet = obVal.getAllValuesWithoutColumns()

        Dim aSQLString As String = "INSERT INTO dokter VALUES(" & kolomSet & ")"

        connect()

        Using aSQLCommand As New SQLiteCommand()
            aSQLCommand.Connection = connection()
            aSQLCommand.CommandText = aSQLString
            aSQLCommand.Prepare()

            aSQLCommand.ExecuteNonQuery()
            stat = True
        End Using

        disconnect()

        Return stat
    End Function


End Module
