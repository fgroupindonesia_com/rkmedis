﻿Public Class Dokter

    Dim columns As String
    Function getIDColumnWithValue() As String
        columns = "id=" & Me.ID
        Return columns
    End Function

    Function getAllValuesWithoutColumns() As String
        ' the ordering is fixed based on SQLite structures
        Return "null, " & DBOperation.Varchar(Me.NIP) & _
            ", " & DBOperation.Varchar(Me.Nama) & _
        ", " & DBOperation.Varchar(Me.Kode) & _
        ", " & DBOperation.Varchar(Me.Spesialis)
    End Function

    Function getAllColumnsWithValues() As String
        columns = "nip=" & DBOperation.Varchar(Me.NIP) & _
           ",nama=" & DBOperation.Varchar(Me.Nama) & _
           ",kode=" & DBOperation.Varchar(Me.Kode) & _
           ",spesialis=" & DBOperation.Varchar(Me.Spesialis)

        Return columns
    End Function


    Private _id As Integer
    Public Property ID() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Private _spesialis As String
    Public Property Spesialis() As String
        Get
            Return _spesialis
        End Get
        Set(ByVal value As String)
            _spesialis = value
        End Set
    End Property


    Private _nip As String
    Public Property NIP() As String
        Get
            Return _nip
        End Get
        Set(ByVal value As String)
            _nip = value
        End Set
    End Property


    Private _kode As String
    Public Property Kode() As String
        Get
            Return _kode
        End Get
        Set(ByVal value As String)
            _kode = value
        End Set
    End Property


    Private _nama As String
    Public Property Nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property


End Class
