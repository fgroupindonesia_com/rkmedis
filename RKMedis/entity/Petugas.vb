﻿Public Class Petugas


    Private columns As String

    Function getIDColumnWithValue() As String
        columns = "id=" & Me.ID
        Return columns
    End Function

    Function getAllColumnsWithValues() As String
        columns = "nip=" & DBOperation.Varchar(Me.NIP) & _
           ",nama=" & DBOperation.Varchar(Me.Nama) & _
           ",jabatan=" & DBOperation.Varchar(Me.Jabatan) & _
           ",telp=" & DBOperation.Varchar(Me.Telp)

        Return columns
    End Function

    Function getAllValuesWithoutColumns() As String
        ' the ordering is fixed based on SQLite structures
        Return "null, " & DBOperation.Varchar(Me.NIP) & _
            ", " & DBOperation.Varchar(Me.Nama) & _
        ", " & DBOperation.Varchar(Me.Jabatan) & _
        ", " & DBOperation.Varchar(Me.Telp)
    End Function

    Private _id As Integer
    Public Property ID() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            _id = value
        End Set
    End Property

    Private _nama As String
    Public Property Nama() As String
        Get
            Return _nama
        End Get
        Set(ByVal value As String)
            _nama = value
        End Set
    End Property

    Private _nip As String
    Public Property NIP() As String
        Get
            Return _nip
        End Get
        Set(ByVal value As String)
            _nip = value
        End Set
    End Property

    Private _telp As String
    Public Property Telp() As String
        Get
            Return _telp
        End Get
        Set(ByVal value As String)
            _telp = value
        End Set
    End Property


    Private _jabatan As String
    Public Property Jabatan() As String
        Get
            Return _jabatan
        End Get
        Set(ByVal value As String)
            _jabatan = value
        End Set
    End Property


End Class
