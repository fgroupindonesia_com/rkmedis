﻿Public Class Users

    Private columns As String

    Function getAllColumnsWithValues() As String

        columns = "passw=" & DBOperation.Varchar(Me.PasswordNew) & _
            ",rec_quest=" & DBOperation.Varchar(Me.RecoveryQuestion) & _
            ",rec_answer=" & DBOperation.Varchar(Me.RecoveryAnswer) & _
            ",logo_filename=" & DBOperation.Varchar(Me.LogoFileName) & _
            ",logo_content=@logo"

        Return columns
    End Function

    Function getUsernameColumnWithValue() As String
        columns = "username=" & DBOperation.Varchar(Me.Username)
        Return columns
    End Function


    Private _username As String
    Public Property Username() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Private _passwNew As String
    Public Property PasswordNew() As String
        Get
            Return _passwNew
        End Get
        Set(ByVal value As String)
            _passwNew = value
        End Set
    End Property

    Private _passwOld As String
    Public Property PasswordOld() As String
        Get
            Return _passwOld
        End Get
        Set(ByVal value As String)
            _passwOld = value
        End Set
    End Property

    Private _passw As String
    Public Property Password() As String
        Get
            Return _passw
        End Get
        Set(ByVal value As String)
            _passw = value
        End Set
    End Property

    Private _recQuest As String
    Public Property RecoveryQuestion() As String
        Get
            Return _recQuest
        End Get
        Set(ByVal value As String)
            _recQuest = value
        End Set
    End Property

    Private _recAnswer As String
    Public Property RecoveryAnswer() As String
        Get
            Return _recAnswer
        End Get
        Set(ByVal value As String)
            _recAnswer = value
        End Set
    End Property

    Private _logo As Byte()
    Public Property Logo() As Byte()
        Get
            Return _logo
        End Get
        Set(ByVal value As Byte())
            _logo = value
        End Set
    End Property

    Private _logoFileName As String
    Public Property LogoFileName() As String
        Get
            Return _logoFileName
        End Get
        Set(ByVal value As String)
            _logoFileName = value
        End Set
    End Property


End Class
