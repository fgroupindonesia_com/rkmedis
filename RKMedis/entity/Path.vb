﻿Imports System.Environment

Module Path

    Dim appName As String = "RK-Medis"
    Dim sysPath As String = GetFolderPath(SpecialFolder.ApplicationData)
    Dim dbFileName As String = "db-used.db"

    Private dbFileLocation As String = sysPath & "\" & appName & "\" & dbFileName
    Public Property ApplicationDBLocation() As String
        Get
            Return dbFileLocation
        End Get
        Set(ByVal value As String)
            dbFileLocation = value
        End Set
    End Property

End Module
