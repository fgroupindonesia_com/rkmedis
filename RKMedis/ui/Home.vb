﻿Public Class Home

    Dim dataPetugas As Petugas
    Dim dataDokter As Dokter

    Private Sub Button_AddPetugas_Click(sender As Object, e As EventArgs) Handles Button_AddPetugas.Click
        activateFormPetugas(True)
        clearFormPetugas()

        dataPetugas = New Petugas()
        Label_ModeFormPetugas.Text = "NEW"

    End Sub

    Private Sub Button_SavePetugas_Click(sender As Object, e As EventArgs) Handles Button_SavePetugas.Click

        dataPetugas.Nama = TextBox_NamaPetugas.Text
        dataPetugas.NIP = TextBox_NipPetugas.Text
        dataPetugas.Telp = TextBox_TelpPetugas.Text
        dataPetugas.Jabatan = TextBox_JabatanPetugas.Text


        If (Label_ModeFormPetugas.Text = "EDIT") Then
            'updating data
            dataPetugas.ID = Label_IDPetugas.Text
            DBOperation.UpdatePetugas(dataPetugas)
        Else
            'saving new data
            DBOperation.SavePetugas(dataPetugas)

        End If

        GUIHelper.renderTablePetugas(DBOperation.GetAllPetugas(), DataGridView_Petugas)

        activateFormPetugas(False)
    End Sub

    Private Sub DataGridView_Petugas_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView_Petugas.ColumnHeaderMouseClick


        If (e.ColumnIndex = 0) Then
            GUIHelper.activateAllCheckboxesTable(DataGridView_Petugas)
        End If
    End Sub


    Private Sub Button_EditPetugas_Click(sender As Object, e As EventArgs) Handles Button_EditPetugas.Click
        dataPetugas = GUIHelper.getCheckedRowTablePetugas(DataGridView_Petugas)

        If (dataPetugas IsNot Nothing) Then

            TextBox_JabatanPetugas.Text = dataPetugas.Jabatan
            TextBox_NamaPetugas.Text = dataPetugas.Nama
            TextBox_NipPetugas.Text = dataPetugas.NIP
            TextBox_TelpPetugas.Text = dataPetugas.Telp
            Label_IDPetugas.Text = dataPetugas.ID

            Label_ModeFormPetugas.Text = "EDIT"

            activateFormPetugas(True)

        Else
            MsgBox("Pilih dulu data petugasnya!")
        End If

    End Sub

    Private Sub DataGridView_Petugas_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView_Petugas.SelectionChanged
        GUIHelper.activateAllCheckboxesSelectedRowTable(DataGridView_Petugas)
    End Sub

    Private Sub clearFormDokter()
        GUIHelper.clearAllTextboxes(TextBox_NipDokter, TextBox_NamaDokter, TextBox_SpesialisDokter, TextBox_KodeDokter)
    End Sub

    Private Sub clearFormPetugas()
        GUIHelper.clearAllTextboxes(TextBox_JabatanPetugas, TextBox_NamaPetugas, TextBox_NipPetugas, TextBox_TelpPetugas)
    End Sub

    Private Sub activateFormPetugas(ByVal stat As Boolean)
        If (stat = True) Then
            GUIHelper.activateAllTextboxes(TextBox_JabatanPetugas, TextBox_NamaPetugas, TextBox_NipPetugas, TextBox_TelpPetugas)
            TextBox_NipPetugas.Focus()
            GUIHelper.activateAllButtons(Button_SavePetugas, Button_CancelPetugas)
        Else
            GUIHelper.disactiveAllTextboxes(TextBox_JabatanPetugas, TextBox_NamaPetugas, TextBox_NipPetugas, TextBox_TelpPetugas)
            GUIHelper.disactiveAllButton(Button_SavePetugas, Button_CancelPetugas)
        End If
        
    End Sub

    Private Sub activateFormDokter(ByVal stat As Boolean)
        If (stat = True) Then
            GUIHelper.activateAllTextboxes(TextBox_NipDokter, TextBox_NamaDokter, TextBox_SpesialisDokter, TextBox_KodeDokter)
            TextBox_NipDokter.Focus()
            GUIHelper.activateAllButtons(Button_SaveDokter, Button_CancelDokter)
        Else
            GUIHelper.disactiveAllTextboxes(TextBox_NipDokter, TextBox_NamaDokter, TextBox_SpesialisDokter, TextBox_KodeDokter)
            GUIHelper.disactiveAllButton(Button_SaveDokter, Button_CancelDokter)
        End If

    End Sub

    Private Sub Button_CancelPetugas_Click(sender As Object, e As EventArgs) Handles Button_CancelPetugas.Click
        activateFormPetugas(False)
    End Sub

    
    Private Sub Home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GUIHelper.renderTablePetugas(DBOperation.GetAllPetugas(), DataGridView_Petugas)
    End Sub

    Private Sub Button_DeletePetugas_Click(sender As Object, e As EventArgs) Handles Button_DeletePetugas.Click
        Dim datList As ArrayList = GUIHelper.getCheckedRowsTablePetugas(DataGridView_Petugas)

        If (datList IsNot Nothing) Then
            For Each satuan As Petugas In datList
                DBOperation.DeletePetugas(satuan)
            Next

            GUIHelper.renderTablePetugas(DBOperation.GetAllPetugas(), DataGridView_Petugas)
        Else
            MsgBox("Pilih dulu datanya!")
        End If
        


    End Sub

    Private Sub Button_ResetPetugas_Click(sender As Object, e As EventArgs) Handles Button_ResetPetugas.Click
        GUIHelper.renderTablePetugas(DBOperation.GetAllPetugas(), DataGridView_Petugas)
    End Sub

   
    Private Sub Button_AddDokter_Click(sender As Object, e As EventArgs) Handles Button_AddDokter.Click
        activateFormDokter(True)
        clearFormDokter()

        dataDokter = New Dokter()
        Label_ModeFormDokter.Text = "NEW"
    End Sub

    Private Sub Button_ResetDokter_Click(sender As Object, e As EventArgs) Handles Button_ResetDokter.Click
        GUIHelper.renderTableDokter(DBOperation.GetAllDokter(), DataGridView_Dokter)
    End Sub

    Private Sub Button_DeleteDokter_Click(sender As Object, e As EventArgs) Handles Button_DeleteDokter.Click
        Dim datList As ArrayList = GUIHelper.getCheckedRowsTableDokter(DataGridView_Dokter)

        If (datList IsNot Nothing) Then
            For Each satuan As Dokter In datList
                DBOperation.DeleteDokter(satuan)
            Next

            GUIHelper.renderTableDokter(DBOperation.GetAllDokter(), DataGridView_Dokter)
        Else
            MsgBox("Pilih dulu datanya!")
        End If

    End Sub

    Private Sub Button_EditDokter_Click(sender As Object, e As EventArgs) Handles Button_EditDokter.Click
        dataDokter = GUIHelper.getCheckedRowTableDokter(DataGridView_Dokter)

        If (dataDokter IsNot Nothing) Then

            TextBox_KodeDokter.Text = dataDokter.Kode
            TextBox_NamaDokter.Text = dataDokter.Nama
            TextBox_NipDokter.Text = dataDokter.NIP
            TextBox_SpesialisDokter.Text = dataDokter.Spesialis
            Label_IDDokter.Text = dataDokter.ID

            Label_ModeFormDokter.Text = "EDIT"

            activateFormDokter(True)

        Else
            MsgBox("Pilih dulu data petugasnya!")
        End If

    End Sub

    Private Sub Button_CancelDokter_Click(sender As Object, e As EventArgs) Handles Button_CancelDokter.Click
        activateFormDokter(False)
    End Sub

    Private Sub Button_SaveDokter_Click(sender As Object, e As EventArgs) Handles Button_SaveDokter.Click


        dataDokter.Nama = TextBox_NamaDokter.Text
        dataDokter.NIP = TextBox_NipDokter.Text
        dataDokter.Kode = TextBox_KodeDokter.Text
        dataDokter.Spesialis = TextBox_SpesialisDokter.Text


        If (Label_ModeFormDokter.Text = "EDIT") Then
            'updating data
            dataDokter.ID = Label_IDDokter.Text
            DBOperation.UpdateDokter(dataDokter)
        Else
            'saving new data
            DBOperation.SaveDokter(dataDokter)

        End If

        GUIHelper.renderTableDokter(DBOperation.GetAllDokter(), DataGridView_Dokter)

        activateFormDokter(False)
    End Sub

    Private Sub DataGridView_Dokter_ColumnHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView_Dokter.ColumnHeaderMouseClick

        If (e.ColumnIndex = 0) Then
            GUIHelper.activateAllCheckboxesTable(DataGridView_Dokter)
        End If
    End Sub

    Private Sub DataGridView_Dokter_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView_Dokter.SelectionChanged
        GUIHelper.activateAllCheckboxesSelectedRowTable(DataGridView_Dokter)
    End Sub
End Class