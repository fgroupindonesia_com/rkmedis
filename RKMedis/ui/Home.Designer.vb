﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Home))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SystemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabDokter = New System.Windows.Forms.TabPage()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox_SpesialisDokter = New System.Windows.Forms.TextBox()
        Me.TextBox_NipDokter = New System.Windows.Forms.TextBox()
        Me.TextBox_NamaDokter = New System.Windows.Forms.TextBox()
        Me.TextBox_KodeDokter = New System.Windows.Forms.TextBox()
        Me.Button_SaveDokter = New System.Windows.Forms.Button()
        Me.Button_CancelDokter = New System.Windows.Forms.Button()
        Me.Label_IDDokter = New System.Windows.Forms.Label()
        Me.Label_ModeFormDokter = New System.Windows.Forms.Label()
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
        Me.Button_ResetDokter = New System.Windows.Forms.Button()
        Me.Button_AddDokter = New System.Windows.Forms.Button()
        Me.Button_DeleteDokter = New System.Windows.Forms.Button()
        Me.Button_EditDokter = New System.Windows.Forms.Button()
        Me.DataGridView_Dokter = New System.Windows.Forms.DataGridView()
        Me.ColumnDokter_Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ColumnDokter_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDokter_NIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDokter_Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDokter_Kode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnDokter_Spesialis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPetugas = New System.Windows.Forms.TabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.TextBox_TelpPetugas = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TextBox_NipPetugas = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox_NamaPetugas = New System.Windows.Forms.TextBox()
        Me.TextBox_JabatanPetugas = New System.Windows.Forms.TextBox()
        Me.Button_SavePetugas = New System.Windows.Forms.Button()
        Me.Button_CancelPetugas = New System.Windows.Forms.Button()
        Me.Label_IDPetugas = New System.Windows.Forms.Label()
        Me.Label_ModeFormPetugas = New System.Windows.Forms.Label()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Button_ResetPetugas = New System.Windows.Forms.Button()
        Me.Button_AddPetugas = New System.Windows.Forms.Button()
        Me.Button_DeletePetugas = New System.Windows.Forms.Button()
        Me.Button_EditPetugas = New System.Windows.Forms.Button()
        Me.DataGridView_Petugas = New System.Windows.Forms.DataGridView()
        Me.ColumnPetugas_Check = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ColumnPetugas_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnPetugas_NIP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnPetugas_Nama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnPetugas_Jabatan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnPetugas_Telp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabHome = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PanelHomeDokter = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PanelHomePetugas = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabPasien = New System.Windows.Forms.TabPage()
        Me.TabPoliklinik = New System.Windows.Forms.TabPage()
        Me.TabPenyimpanan = New System.Windows.Forms.TabPage()
        Me.MenuStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TabDokter.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.Panel2.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        CType(Me.DataGridView_Dokter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPetugas.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.DataGridView_Petugas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabHome.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelHomeDokter.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelHomePetugas.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.SettingsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(983, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatabaseToolStripMenuItem, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'DatabaseToolStripMenuItem
        '
        Me.DatabaseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportToolStripMenuItem, Me.ImportToolStripMenuItem})
        Me.DatabaseToolStripMenuItem.Name = "DatabaseToolStripMenuItem"
        Me.DatabaseToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.DatabaseToolStripMenuItem.Text = "Database"
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.ExportToolStripMenuItem.Text = "Export"
        '
        'ImportToolStripMenuItem
        '
        Me.ImportToolStripMenuItem.Name = "ImportToolStripMenuItem"
        Me.ImportToolStripMenuItem.Size = New System.Drawing.Size(110, 22)
        Me.ImportToolStripMenuItem.Text = "Import"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(119, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SystemToolStripMenuItem})
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'SystemToolStripMenuItem
        '
        Me.SystemToolStripMenuItem.Name = "SystemToolStripMenuItem"
        Me.SystemToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.SystemToolStripMenuItem.Text = "System"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripSeparator2, Me.ToolStripLabel1, Me.ToolStripTextBox1, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(983, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(78, 22)
        Me.ToolStripLabel1.Text = "Search Data : "
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(100, 25)
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Clear"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 680)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(983, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'TabDokter
        '
        Me.TabDokter.Controls.Add(Me.SplitContainer3)
        Me.TabDokter.Location = New System.Drawing.Point(4, 22)
        Me.TabDokter.Name = "TabDokter"
        Me.TabDokter.Padding = New System.Windows.Forms.Padding(3)
        Me.TabDokter.Size = New System.Drawing.Size(975, 605)
        Me.TabDokter.TabIndex = 1
        Me.TabDokter.Text = "Dokter"
        Me.TabDokter.UseVisualStyleBackColor = True
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.GroupBox6)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.SplitContainer4)
        Me.SplitContainer3.Size = New System.Drawing.Size(969, 599)
        Me.SplitContainer3.SplitterDistance = 343
        Me.SplitContainer3.TabIndex = 1
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TableLayoutPanel6)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox6.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(343, 599)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Data"
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.33083!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.66917!))
        Me.TableLayoutPanel6.Controls.Add(Me.Label15, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.Label14, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Label16, 0, 2)
        Me.TableLayoutPanel6.Controls.Add(Me.Label17, 0, 3)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox_SpesialisDokter, 1, 3)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox_NipDokter, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox_NamaDokter, 1, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.TextBox_KodeDokter, 1, 2)
        Me.TableLayoutPanel6.Controls.Add(Me.Button_SaveDokter, 0, 4)
        Me.TableLayoutPanel6.Controls.Add(Me.Button_CancelDokter, 1, 4)
        Me.TableLayoutPanel6.Controls.Add(Me.Label_IDDokter, 0, 5)
        Me.TableLayoutPanel6.Controls.Add(Me.Label_ModeFormDokter, 1, 5)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(35, 44)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 6
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(266, 178)
        Me.TableLayoutPanel6.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 27)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(44, 13)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Nama : "
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(31, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "NIP :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(3, 54)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(38, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Kode :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 81)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(54, 13)
        Me.Label17.TabIndex = 4
        Me.Label17.Text = "Spesialis :"
        '
        'TextBox_SpesialisDokter
        '
        Me.TextBox_SpesialisDokter.Enabled = False
        Me.TextBox_SpesialisDokter.Location = New System.Drawing.Point(89, 84)
        Me.TextBox_SpesialisDokter.Name = "TextBox_SpesialisDokter"
        Me.TextBox_SpesialisDokter.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_SpesialisDokter.TabIndex = 10
        '
        'TextBox_NipDokter
        '
        Me.TextBox_NipDokter.Enabled = False
        Me.TextBox_NipDokter.Location = New System.Drawing.Point(89, 3)
        Me.TextBox_NipDokter.Name = "TextBox_NipDokter"
        Me.TextBox_NipDokter.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_NipDokter.TabIndex = 1
        '
        'TextBox_NamaDokter
        '
        Me.TextBox_NamaDokter.Enabled = False
        Me.TextBox_NamaDokter.Location = New System.Drawing.Point(89, 30)
        Me.TextBox_NamaDokter.Name = "TextBox_NamaDokter"
        Me.TextBox_NamaDokter.Size = New System.Drawing.Size(150, 20)
        Me.TextBox_NamaDokter.TabIndex = 6
        '
        'TextBox_KodeDokter
        '
        Me.TextBox_KodeDokter.Enabled = False
        Me.TextBox_KodeDokter.Location = New System.Drawing.Point(89, 57)
        Me.TextBox_KodeDokter.Name = "TextBox_KodeDokter"
        Me.TextBox_KodeDokter.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_KodeDokter.TabIndex = 7
        '
        'Button_SaveDokter
        '
        Me.Button_SaveDokter.Enabled = False
        Me.Button_SaveDokter.Location = New System.Drawing.Point(3, 111)
        Me.Button_SaveDokter.Name = "Button_SaveDokter"
        Me.Button_SaveDokter.Size = New System.Drawing.Size(75, 23)
        Me.Button_SaveDokter.TabIndex = 9
        Me.Button_SaveDokter.Text = "Save"
        Me.Button_SaveDokter.UseVisualStyleBackColor = True
        '
        'Button_CancelDokter
        '
        Me.Button_CancelDokter.Enabled = False
        Me.Button_CancelDokter.Location = New System.Drawing.Point(89, 111)
        Me.Button_CancelDokter.Name = "Button_CancelDokter"
        Me.Button_CancelDokter.Size = New System.Drawing.Size(75, 23)
        Me.Button_CancelDokter.TabIndex = 8
        Me.Button_CancelDokter.Text = "Cancel"
        Me.Button_CancelDokter.UseVisualStyleBackColor = True
        '
        'Label_IDDokter
        '
        Me.Label_IDDokter.AutoSize = True
        Me.Label_IDDokter.Location = New System.Drawing.Point(3, 155)
        Me.Label_IDDokter.Name = "Label_IDDokter"
        Me.Label_IDDokter.Size = New System.Drawing.Size(50, 13)
        Me.Label_IDDokter.TabIndex = 11
        Me.Label_IDDokter.Text = "IDDokter"
        Me.Label_IDDokter.Visible = False
        '
        'Label_ModeFormDokter
        '
        Me.Label_ModeFormDokter.AutoSize = True
        Me.Label_ModeFormDokter.Location = New System.Drawing.Point(89, 155)
        Me.Label_ModeFormDokter.Name = "Label_ModeFormDokter"
        Me.Label_ModeFormDokter.Size = New System.Drawing.Size(34, 13)
        Me.Label_ModeFormDokter.TabIndex = 12
        Me.Label_ModeFormDokter.Text = "Mode"
        Me.Label_ModeFormDokter.Visible = False
        '
        'SplitContainer4
        '
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.IsSplitterFixed = True
        Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer4.Name = "SplitContainer4"
        Me.SplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.Button_ResetDokter)
        Me.SplitContainer4.Panel1.Controls.Add(Me.Button_AddDokter)
        Me.SplitContainer4.Panel1.Controls.Add(Me.Button_DeleteDokter)
        Me.SplitContainer4.Panel1.Controls.Add(Me.Button_EditDokter)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.Controls.Add(Me.DataGridView_Dokter)
        Me.SplitContainer4.Size = New System.Drawing.Size(622, 599)
        Me.SplitContainer4.SplitterDistance = 59
        Me.SplitContainer4.TabIndex = 0
        '
        'Button_ResetDokter
        '
        Me.Button_ResetDokter.Location = New System.Drawing.Point(572, 24)
        Me.Button_ResetDokter.Name = "Button_ResetDokter"
        Me.Button_ResetDokter.Size = New System.Drawing.Size(39, 23)
        Me.Button_ResetDokter.TabIndex = 3
        Me.Button_ResetDokter.Text = "(*)"
        Me.Button_ResetDokter.UseVisualStyleBackColor = True
        '
        'Button_AddDokter
        '
        Me.Button_AddDokter.Location = New System.Drawing.Point(322, 24)
        Me.Button_AddDokter.Name = "Button_AddDokter"
        Me.Button_AddDokter.Size = New System.Drawing.Size(75, 23)
        Me.Button_AddDokter.TabIndex = 2
        Me.Button_AddDokter.Text = "Add"
        Me.Button_AddDokter.UseVisualStyleBackColor = True
        '
        'Button_DeleteDokter
        '
        Me.Button_DeleteDokter.Location = New System.Drawing.Point(491, 24)
        Me.Button_DeleteDokter.Name = "Button_DeleteDokter"
        Me.Button_DeleteDokter.Size = New System.Drawing.Size(75, 23)
        Me.Button_DeleteDokter.TabIndex = 1
        Me.Button_DeleteDokter.Text = "Delete"
        Me.Button_DeleteDokter.UseVisualStyleBackColor = True
        '
        'Button_EditDokter
        '
        Me.Button_EditDokter.Location = New System.Drawing.Point(410, 24)
        Me.Button_EditDokter.Name = "Button_EditDokter"
        Me.Button_EditDokter.Size = New System.Drawing.Size(75, 23)
        Me.Button_EditDokter.TabIndex = 0
        Me.Button_EditDokter.Text = "Edit"
        Me.Button_EditDokter.UseVisualStyleBackColor = True
        '
        'DataGridView_Dokter
        '
        Me.DataGridView_Dokter.AllowUserToAddRows = False
        Me.DataGridView_Dokter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView_Dokter.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnDokter_Check, Me.ColumnDokter_ID, Me.ColumnDokter_NIP, Me.ColumnDokter_Nama, Me.ColumnDokter_Kode, Me.ColumnDokter_Spesialis})
        Me.DataGridView_Dokter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView_Dokter.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView_Dokter.Name = "DataGridView_Dokter"
        Me.DataGridView_Dokter.Size = New System.Drawing.Size(622, 536)
        Me.DataGridView_Dokter.TabIndex = 0
        '
        'ColumnDokter_Check
        '
        Me.ColumnDokter_Check.HeaderText = "#"
        Me.ColumnDokter_Check.Name = "ColumnDokter_Check"
        Me.ColumnDokter_Check.Width = 25
        '
        'ColumnDokter_ID
        '
        Me.ColumnDokter_ID.HeaderText = "#"
        Me.ColumnDokter_ID.Name = "ColumnDokter_ID"
        Me.ColumnDokter_ID.Visible = False
        '
        'ColumnDokter_NIP
        '
        Me.ColumnDokter_NIP.HeaderText = "NIP"
        Me.ColumnDokter_NIP.Name = "ColumnDokter_NIP"
        Me.ColumnDokter_NIP.Width = 45
        '
        'ColumnDokter_Nama
        '
        Me.ColumnDokter_Nama.HeaderText = "Nama"
        Me.ColumnDokter_Nama.Name = "ColumnDokter_Nama"
        '
        'ColumnDokter_Kode
        '
        Me.ColumnDokter_Kode.HeaderText = "Kode"
        Me.ColumnDokter_Kode.Name = "ColumnDokter_Kode"
        '
        'ColumnDokter_Spesialis
        '
        Me.ColumnDokter_Spesialis.HeaderText = "Spesialis"
        Me.ColumnDokter_Spesialis.Name = "ColumnDokter_Spesialis"
        '
        'TabPetugas
        '
        Me.TabPetugas.Controls.Add(Me.SplitContainer1)
        Me.TabPetugas.Location = New System.Drawing.Point(4, 22)
        Me.TabPetugas.Name = "TabPetugas"
        Me.TabPetugas.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPetugas.Size = New System.Drawing.Size(975, 605)
        Me.TabPetugas.TabIndex = 0
        Me.TabPetugas.Text = "Petugas"
        Me.TabPetugas.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox5)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Size = New System.Drawing.Size(969, 599)
        Me.SplitContainer1.SplitterDistance = 343
        Me.SplitContainer1.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TableLayoutPanel5)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox5.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(343, 599)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Data"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.33083!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.66917!))
        Me.TableLayoutPanel5.Controls.Add(Me.TextBox_TelpPetugas, 1, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.Label10, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.TextBox_NipPetugas, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Label11, 0, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.Label12, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.Label13, 0, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.TextBox_NamaPetugas, 1, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.TextBox_JabatanPetugas, 1, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.Button_SavePetugas, 0, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.Button_CancelPetugas, 1, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.Label_IDPetugas, 0, 5)
        Me.TableLayoutPanel5.Controls.Add(Me.Label_ModeFormPetugas, 1, 5)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(35, 44)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 6
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(266, 178)
        Me.TableLayoutPanel5.TabIndex = 0
        '
        'TextBox_TelpPetugas
        '
        Me.TextBox_TelpPetugas.Enabled = False
        Me.TextBox_TelpPetugas.Location = New System.Drawing.Point(89, 84)
        Me.TextBox_TelpPetugas.Name = "TextBox_TelpPetugas"
        Me.TextBox_TelpPetugas.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_TelpPetugas.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "NIP :"
        '
        'TextBox_NipPetugas
        '
        Me.TextBox_NipPetugas.Enabled = False
        Me.TextBox_NipPetugas.Location = New System.Drawing.Point(89, 3)
        Me.TextBox_NipPetugas.Name = "TextBox_NipPetugas"
        Me.TextBox_NipPetugas.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_NipPetugas.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(44, 13)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Nama : "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 54)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(54, 13)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Jabatan : "
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 81)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(37, 13)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "Telp : "
        '
        'TextBox_NamaPetugas
        '
        Me.TextBox_NamaPetugas.Enabled = False
        Me.TextBox_NamaPetugas.Location = New System.Drawing.Point(89, 30)
        Me.TextBox_NamaPetugas.Name = "TextBox_NamaPetugas"
        Me.TextBox_NamaPetugas.Size = New System.Drawing.Size(150, 20)
        Me.TextBox_NamaPetugas.TabIndex = 6
        '
        'TextBox_JabatanPetugas
        '
        Me.TextBox_JabatanPetugas.Enabled = False
        Me.TextBox_JabatanPetugas.Location = New System.Drawing.Point(89, 57)
        Me.TextBox_JabatanPetugas.Name = "TextBox_JabatanPetugas"
        Me.TextBox_JabatanPetugas.Size = New System.Drawing.Size(129, 20)
        Me.TextBox_JabatanPetugas.TabIndex = 7
        '
        'Button_SavePetugas
        '
        Me.Button_SavePetugas.Enabled = False
        Me.Button_SavePetugas.Location = New System.Drawing.Point(3, 111)
        Me.Button_SavePetugas.Name = "Button_SavePetugas"
        Me.Button_SavePetugas.Size = New System.Drawing.Size(75, 23)
        Me.Button_SavePetugas.TabIndex = 9
        Me.Button_SavePetugas.Text = "Save"
        Me.Button_SavePetugas.UseVisualStyleBackColor = True
        '
        'Button_CancelPetugas
        '
        Me.Button_CancelPetugas.Enabled = False
        Me.Button_CancelPetugas.Location = New System.Drawing.Point(89, 111)
        Me.Button_CancelPetugas.Name = "Button_CancelPetugas"
        Me.Button_CancelPetugas.Size = New System.Drawing.Size(75, 23)
        Me.Button_CancelPetugas.TabIndex = 8
        Me.Button_CancelPetugas.Text = "Cancel"
        Me.Button_CancelPetugas.UseVisualStyleBackColor = True
        '
        'Label_IDPetugas
        '
        Me.Label_IDPetugas.AutoSize = True
        Me.Label_IDPetugas.Location = New System.Drawing.Point(3, 155)
        Me.Label_IDPetugas.Name = "Label_IDPetugas"
        Me.Label_IDPetugas.Size = New System.Drawing.Size(57, 13)
        Me.Label_IDPetugas.TabIndex = 11
        Me.Label_IDPetugas.Text = "IDPetugas"
        Me.Label_IDPetugas.Visible = False
        '
        'Label_ModeFormPetugas
        '
        Me.Label_ModeFormPetugas.AutoSize = True
        Me.Label_ModeFormPetugas.Location = New System.Drawing.Point(89, 155)
        Me.Label_ModeFormPetugas.Name = "Label_ModeFormPetugas"
        Me.Label_ModeFormPetugas.Size = New System.Drawing.Size(34, 13)
        Me.Label_ModeFormPetugas.TabIndex = 12
        Me.Label_ModeFormPetugas.Text = "Mode"
        Me.Label_ModeFormPetugas.Visible = False
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button_ResetPetugas)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button_AddPetugas)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button_DeletePetugas)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Button_EditPetugas)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.DataGridView_Petugas)
        Me.SplitContainer2.Size = New System.Drawing.Size(622, 599)
        Me.SplitContainer2.SplitterDistance = 59
        Me.SplitContainer2.TabIndex = 0
        '
        'Button_ResetPetugas
        '
        Me.Button_ResetPetugas.Location = New System.Drawing.Point(572, 24)
        Me.Button_ResetPetugas.Name = "Button_ResetPetugas"
        Me.Button_ResetPetugas.Size = New System.Drawing.Size(39, 23)
        Me.Button_ResetPetugas.TabIndex = 3
        Me.Button_ResetPetugas.Text = "(*)"
        Me.Button_ResetPetugas.UseVisualStyleBackColor = True
        '
        'Button_AddPetugas
        '
        Me.Button_AddPetugas.Location = New System.Drawing.Point(322, 24)
        Me.Button_AddPetugas.Name = "Button_AddPetugas"
        Me.Button_AddPetugas.Size = New System.Drawing.Size(75, 23)
        Me.Button_AddPetugas.TabIndex = 2
        Me.Button_AddPetugas.Text = "Add"
        Me.Button_AddPetugas.UseVisualStyleBackColor = True
        '
        'Button_DeletePetugas
        '
        Me.Button_DeletePetugas.Location = New System.Drawing.Point(491, 24)
        Me.Button_DeletePetugas.Name = "Button_DeletePetugas"
        Me.Button_DeletePetugas.Size = New System.Drawing.Size(75, 23)
        Me.Button_DeletePetugas.TabIndex = 1
        Me.Button_DeletePetugas.Text = "Delete"
        Me.Button_DeletePetugas.UseVisualStyleBackColor = True
        '
        'Button_EditPetugas
        '
        Me.Button_EditPetugas.Location = New System.Drawing.Point(410, 24)
        Me.Button_EditPetugas.Name = "Button_EditPetugas"
        Me.Button_EditPetugas.Size = New System.Drawing.Size(75, 23)
        Me.Button_EditPetugas.TabIndex = 0
        Me.Button_EditPetugas.Text = "Edit"
        Me.Button_EditPetugas.UseVisualStyleBackColor = True
        '
        'DataGridView_Petugas
        '
        Me.DataGridView_Petugas.AllowUserToAddRows = False
        Me.DataGridView_Petugas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView_Petugas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColumnPetugas_Check, Me.ColumnPetugas_ID, Me.ColumnPetugas_NIP, Me.ColumnPetugas_Nama, Me.ColumnPetugas_Jabatan, Me.ColumnPetugas_Telp})
        Me.DataGridView_Petugas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView_Petugas.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView_Petugas.Name = "DataGridView_Petugas"
        Me.DataGridView_Petugas.Size = New System.Drawing.Size(622, 536)
        Me.DataGridView_Petugas.TabIndex = 0
        '
        'ColumnPetugas_Check
        '
        Me.ColumnPetugas_Check.HeaderText = "#"
        Me.ColumnPetugas_Check.Name = "ColumnPetugas_Check"
        Me.ColumnPetugas_Check.Width = 25
        '
        'ColumnPetugas_ID
        '
        Me.ColumnPetugas_ID.HeaderText = "#"
        Me.ColumnPetugas_ID.Name = "ColumnPetugas_ID"
        Me.ColumnPetugas_ID.Visible = False
        '
        'ColumnPetugas_NIP
        '
        Me.ColumnPetugas_NIP.HeaderText = "NIP"
        Me.ColumnPetugas_NIP.Name = "ColumnPetugas_NIP"
        Me.ColumnPetugas_NIP.Width = 45
        '
        'ColumnPetugas_Nama
        '
        Me.ColumnPetugas_Nama.HeaderText = "Nama"
        Me.ColumnPetugas_Nama.Name = "ColumnPetugas_Nama"
        '
        'ColumnPetugas_Jabatan
        '
        Me.ColumnPetugas_Jabatan.HeaderText = "Jabatan"
        Me.ColumnPetugas_Jabatan.Name = "ColumnPetugas_Jabatan"
        '
        'ColumnPetugas_Telp
        '
        Me.ColumnPetugas_Telp.HeaderText = "Telp"
        Me.ColumnPetugas_Telp.Name = "ColumnPetugas_Telp"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabHome)
        Me.TabControl1.Controls.Add(Me.TabPetugas)
        Me.TabControl1.Controls.Add(Me.TabDokter)
        Me.TabControl1.Controls.Add(Me.TabPasien)
        Me.TabControl1.Controls.Add(Me.TabPoliklinik)
        Me.TabControl1.Controls.Add(Me.TabPenyimpanan)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 49)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(983, 631)
        Me.TabControl1.TabIndex = 4
        '
        'TabHome
        '
        Me.TabHome.Controls.Add(Me.Panel2)
        Me.TabHome.Controls.Add(Me.Panel1)
        Me.TabHome.Controls.Add(Me.PanelHomeDokter)
        Me.TabHome.Controls.Add(Me.PanelHomePetugas)
        Me.TabHome.Controls.Add(Me.Label3)
        Me.TabHome.Location = New System.Drawing.Point(4, 22)
        Me.TabHome.Name = "TabHome"
        Me.TabHome.Size = New System.Drawing.Size(975, 605)
        Me.TabHome.TabIndex = 5
        Me.TabHome.Text = "Home"
        Me.TabHome.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GroupBox4)
        Me.Panel2.Controls.Add(Me.PictureBox4)
        Me.Panel2.Location = New System.Drawing.Point(505, 304)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(422, 213)
        Me.Panel2.TabIndex = 6
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.TableLayoutPanel4)
        Me.GroupBox4.Location = New System.Drawing.Point(141, 21)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(258, 167)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Pasien"
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label8, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label9, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Button7, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Button8, 0, 2)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 3
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.25926!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.74074!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(252, 148)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(129, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "100 Data"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Total :"
        '
        'Button7
        '
        Me.Button7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button7.Location = New System.Drawing.Point(3, 71)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(120, 23)
        Me.Button7.TabIndex = 2
        Me.Button7.Text = "Add Data"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button8.Location = New System.Drawing.Point(3, 114)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(120, 23)
        Me.Button8.TabIndex = 3
        Me.Button8.Text = "Report"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'PictureBox4
        '
        Me.PictureBox4.Location = New System.Drawing.Point(7, 21)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(128, 128)
        Me.PictureBox4.TabIndex = 2
        Me.PictureBox4.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.PictureBox3)
        Me.Panel1.Location = New System.Drawing.Point(53, 304)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(422, 213)
        Me.Panel1.TabIndex = 5
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TableLayoutPanel3)
        Me.GroupBox3.Location = New System.Drawing.Point(141, 21)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(258, 167)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Poliklinik"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Label6, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label7, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button5, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button6, 0, 2)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.25926!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.74074!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(252, 148)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(129, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "100 Data"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Total :"
        '
        'Button5
        '
        Me.Button5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button5.Location = New System.Drawing.Point(3, 71)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 23)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "Add Data"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button6.Location = New System.Drawing.Point(3, 114)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 23)
        Me.Button6.TabIndex = 3
        Me.Button6.Text = "Report"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Location = New System.Drawing.Point(7, 21)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(128, 128)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'PanelHomeDokter
        '
        Me.PanelHomeDokter.Controls.Add(Me.GroupBox2)
        Me.PanelHomeDokter.Controls.Add(Me.PictureBox2)
        Me.PanelHomeDokter.Location = New System.Drawing.Point(53, 61)
        Me.PanelHomeDokter.Name = "PanelHomeDokter"
        Me.PanelHomeDokter.Size = New System.Drawing.Size(422, 213)
        Me.PanelHomeDokter.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox2.Location = New System.Drawing.Point(141, 21)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(258, 167)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Dokter"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Button3, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Button4, 0, 2)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.25926!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.74074!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(252, 148)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(129, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "100 Data"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Total :"
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.Location = New System.Drawing.Point(3, 71)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 23)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Add Data"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(3, 114)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 23)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "Report"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(7, 21)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(128, 128)
        Me.PictureBox2.TabIndex = 2
        Me.PictureBox2.TabStop = False
        '
        'PanelHomePetugas
        '
        Me.PanelHomePetugas.Controls.Add(Me.GroupBox1)
        Me.PanelHomePetugas.Controls.Add(Me.PictureBox1)
        Me.PanelHomePetugas.Location = New System.Drawing.Point(505, 61)
        Me.PanelHomePetugas.Name = "PanelHomePetugas"
        Me.PanelHomePetugas.Size = New System.Drawing.Size(422, 213)
        Me.PanelHomePetugas.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.Location = New System.Drawing.Point(141, 21)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(258, 167)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Petugas"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Button1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Button2, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.25926!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.74074!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(252, 148)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(129, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "100 Data"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total :"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(3, 71)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(120, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Add Data"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(3, 114)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Report"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(7, 21)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(128, 128)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(366, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(242, 25)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Home"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPasien
        '
        Me.TabPasien.Location = New System.Drawing.Point(4, 22)
        Me.TabPasien.Name = "TabPasien"
        Me.TabPasien.Size = New System.Drawing.Size(975, 605)
        Me.TabPasien.TabIndex = 2
        Me.TabPasien.Text = "Pasien"
        Me.TabPasien.UseVisualStyleBackColor = True
        '
        'TabPoliklinik
        '
        Me.TabPoliklinik.Location = New System.Drawing.Point(4, 22)
        Me.TabPoliklinik.Name = "TabPoliklinik"
        Me.TabPoliklinik.Size = New System.Drawing.Size(975, 605)
        Me.TabPoliklinik.TabIndex = 3
        Me.TabPoliklinik.Text = "Poliklinik"
        Me.TabPoliklinik.UseVisualStyleBackColor = True
        '
        'TabPenyimpanan
        '
        Me.TabPenyimpanan.Location = New System.Drawing.Point(4, 22)
        Me.TabPenyimpanan.Name = "TabPenyimpanan"
        Me.TabPenyimpanan.Size = New System.Drawing.Size(975, 605)
        Me.TabPenyimpanan.TabIndex = 4
        Me.TabPenyimpanan.Text = "Penyimpanan"
        Me.TabPenyimpanan.UseVisualStyleBackColor = True
        '
        'Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(983, 702)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Home"
        Me.Text = "Home"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabDokter.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer4.ResumeLayout(False)
        CType(Me.DataGridView_Dokter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPetugas.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.DataGridView_Petugas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabHome.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelHomeDokter.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelHomePetugas.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SystemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TabDokter As System.Windows.Forms.TabPage
    Friend WithEvents TabPetugas As System.Windows.Forms.TabPage
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabHome As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPasien As System.Windows.Forms.TabPage
    Friend WithEvents TabPoliklinik As System.Windows.Forms.TabPage
    Friend WithEvents TabPenyimpanan As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents PanelHomePetugas As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanelHomeDokter As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button_AddPetugas As System.Windows.Forms.Button
    Friend WithEvents Button_DeletePetugas As System.Windows.Forms.Button
    Friend WithEvents Button_EditPetugas As System.Windows.Forms.Button
    Friend WithEvents DataGridView_Petugas As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox_NipPetugas As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox_NamaPetugas As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_JabatanPetugas As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_TelpPetugas As System.Windows.Forms.TextBox
    Friend WithEvents Button_SavePetugas As System.Windows.Forms.Button
    Friend WithEvents Button_CancelPetugas As System.Windows.Forms.Button
    Friend WithEvents Button_ResetPetugas As System.Windows.Forms.Button
    Friend WithEvents Label_IDPetugas As System.Windows.Forms.Label
    Friend WithEvents ColumnPetugas_Check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ColumnPetugas_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnPetugas_NIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnPetugas_Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnPetugas_Jabatan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnPetugas_Telp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label_ModeFormPetugas As System.Windows.Forms.Label
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TextBox_SpesialisDokter As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox_NipDokter As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox_NamaDokter As System.Windows.Forms.TextBox
    Friend WithEvents TextBox_KodeDokter As System.Windows.Forms.TextBox
    Friend WithEvents Button_SaveDokter As System.Windows.Forms.Button
    Friend WithEvents Button_CancelDokter As System.Windows.Forms.Button
    Friend WithEvents Label_IDDokter As System.Windows.Forms.Label
    Friend WithEvents Label_ModeFormDokter As System.Windows.Forms.Label
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents Button_ResetDokter As System.Windows.Forms.Button
    Friend WithEvents Button_AddDokter As System.Windows.Forms.Button
    Friend WithEvents Button_DeleteDokter As System.Windows.Forms.Button
    Friend WithEvents Button_EditDokter As System.Windows.Forms.Button
    Friend WithEvents DataGridView_Dokter As System.Windows.Forms.DataGridView
    Friend WithEvents ColumnDokter_Check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ColumnDokter_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDokter_NIP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDokter_Nama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDokter_Kode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnDokter_Spesialis As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
