﻿Module GUIHelper

    Function getCheckedRowsTablePetugas(ByVal aDataGridView As DataGridView) As ArrayList
        Dim aList As ArrayList = Nothing

        Dim aStat As Boolean
        Dim dataPetugas As Petugas

        For Each satuBaris As DataGridViewRow In aDataGridView.Rows
            aStat = Convert.ToBoolean(satuBaris.Cells(0).Value)

            If (aStat = True) Then

                If (aList Is Nothing) Then
                    aList = New ArrayList
                End If

                dataPetugas = New Petugas()

                dataPetugas.ID = satuBaris.Cells(1).Value
                aList.Add(dataPetugas)

            End If
        Next

        Return aList
    End Function

    Function getCheckedRowsTableDokter(ByVal aDataGridView As DataGridView) As ArrayList
        Dim aList As ArrayList = Nothing

        Dim aStat As Boolean
        Dim dataDokter As Dokter

        For Each satuBaris As DataGridViewRow In aDataGridView.Rows
            aStat = Convert.ToBoolean(satuBaris.Cells(0).Value)

            If (aStat = True) Then

                If (aList Is Nothing) Then
                    aList = New ArrayList
                End If

                dataDokter = New Dokter()

                dataDokter.ID = satuBaris.Cells(1).Value
                aList.Add(dataDokter)

            End If
        Next

        Return aList
    End Function

    Function getCheckedRowTablePetugas(ByVal aDataGridView As DataGridView) As Petugas
        Dim aStat As Boolean
        Dim dataSatuPetugas As Petugas = Nothing
        For Each satuBaris As DataGridViewRow In aDataGridView.Rows
            aStat = Convert.ToBoolean(satuBaris.Cells(0).Value)

            If (aStat = True) Then
                dataSatuPetugas = New Petugas()
                dataSatuPetugas.ID = satuBaris.Cells(1).Value
                dataSatuPetugas.NIP = satuBaris.Cells(2).Value
                dataSatuPetugas.Nama = satuBaris.Cells(3).Value
                dataSatuPetugas.Jabatan = satuBaris.Cells(4).Value
                dataSatuPetugas.Telp = satuBaris.Cells(5).Value

                Exit For
            End If
        Next

        Return dataSatuPetugas

    End Function


    Function getCheckedRowTableDokter(ByVal aDataGridView As DataGridView) As Dokter
        Dim aStat As Boolean
        Dim dataSatuDokter As Dokter = Nothing
        For Each satuBaris As DataGridViewRow In aDataGridView.Rows
            aStat = Convert.ToBoolean(satuBaris.Cells(0).Value)

            If (aStat = True) Then
                dataSatuDokter = New Dokter()
                dataSatuDokter.ID = satuBaris.Cells(1).Value
                dataSatuDokter.NIP = satuBaris.Cells(2).Value
                dataSatuDokter.Nama = satuBaris.Cells(3).Value
                dataSatuDokter.Kode = satuBaris.Cells(4).Value
                dataSatuDokter.Spesialis = satuBaris.Cells(5).Value

                Exit For
            End If
        Next

        Return dataSatuDokter

    End Function

    Sub activateAllCheckboxesSelectedRowTable(ByVal aDataGrid As DataGridView)
        'ensuring there is no editable datagridview active
        aDataGrid.EndEdit()

        For Each aRow As DataGridViewRow In aDataGrid.SelectedRows
            aRow.Cells(0).Value = True
        Next

    End Sub

    Sub activateAllCheckboxesTable(ByVal aDataGrid As DataGridView)
        'ensuring there is no editable datagridview active
        aDataGrid.EndEdit()

        Dim stat As Boolean
        For Each aRow As DataGridViewRow In aDataGrid.Rows
            stat = Not Convert.ToBoolean(aRow.Cells(0).Value)
            aRow.Cells(0).Value = stat
        Next

    End Sub

    Sub renderTableDokter(ByVal dataAsList As ArrayList, ByVal aTable As DataGridView)

        aTable.Rows.Clear()

        For Each satuOrang As Dokter In dataAsList
            Dim row As String() = New String() {False, satuOrang.ID, satuOrang.NIP, satuOrang.Nama, satuOrang.Kode, satuOrang.Spesialis}
            aTable.Rows.Add(row)
        Next

    End Sub

    Sub renderTablePetugas(ByVal dataAsList As ArrayList, ByVal aTable As DataGridView)

        aTable.Rows.Clear()

        For Each satuOrang As Petugas In dataAsList
            Dim row As String() = New String() {False, satuOrang.ID, satuOrang.NIP, satuOrang.Nama, satuOrang.Jabatan, satuOrang.Telp}
            aTable.Rows.Add(row)
        Next

    End Sub

    Sub activateAllTextboxes(ByVal ParamArray aComponent As TextBox())
        For Each singleComp As TextBox In aComponent
            singleComp.Enabled = True
        Next
    End Sub

    Sub disactiveAllTextboxes(ByVal ParamArray aComponent As TextBox())
        For Each singleComp As TextBox In aComponent
            singleComp.Enabled = False
        Next
    End Sub

    Sub activateAllButtons(ByVal ParamArray aComponent As Button())
        For Each singleComp As Button In aComponent
            singleComp.Enabled = True
        Next
    End Sub

    Sub disactiveAllButton(ByVal ParamArray aComponent As Button())
        For Each singleComp As Button In aComponent
            singleComp.Enabled = False
        Next
    End Sub

    Sub clearAllTextboxes(ByVal ParamArray aComp As TextBox())
        For Each singleComp As TextBox In aComp
            singleComp.Text = ""
        Next
    End Sub

End Module
